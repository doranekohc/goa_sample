package main

import (
	"bitbucket.org/yoshii/goa_sample/app"
	"github.com/goadesign/goa"
)

// FoobarController implements the foobar resource.
type FoobarController struct {
	*goa.Controller
}

// NewFoobarController creates a foobar controller.
func NewFoobarController(service *goa.Service) *FoobarController {
	return &FoobarController{Controller: service.NewController("FoobarController")}
}

// Show runs the show action.
func (c *FoobarController) Show(ctx *app.ShowFoobarContext) error {
	// FoobarController_Show: start_implement

	// Put your logic here

	return nil
	// FoobarController_Show: end_implement
}
