package main

import (
	"bitbucket.org/yoshii/goa_sample/app"
	"github.com/goadesign/goa"
)

// BottlesController implements the bottles resource.
type BottlesController struct {
	*goa.Controller
}

// NewBottlesController creates a bottles controller.
func NewBottlesController(service *goa.Service) *BottlesController {
	return &BottlesController{Controller: service.NewController("BottlesController")}
}

// Show runs the show action.
func (c *BottlesController) Show(ctx *app.ShowBottlesContext) error {
	// BottlesController_Show: start_implement

	// Put your logic here

	res := &app.GoaExampleBottle{}
	return ctx.OK(res)
	// BottlesController_Show: end_implement
}
