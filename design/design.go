package design // The convention consists of naming the design
// package "design"
import (
	"github.com/goadesign/goa/design" // Use . imports to enable the DSL
	"github.com/goadesign/goa/design/apidsl"
)

var _ = apidsl.API("cellar", func() { // API defines the microservice endpoint and
	apidsl.Title("The virtual wine cellar")    // other global properties. There should be one
	apidsl.Description("A simple goa service") // and exactly one API definition appearing in
	apidsl.Scheme("http")                      // the design.
	apidsl.Host("localhost:8080")
})

var _ = apidsl.Resource("bottle", func() { // Resources group related API endpoints
	apidsl.BasePath("/bottles")      // together. They map to REST resources for REST
	apidsl.DefaultMedia(BottleMedia) // services.

	apidsl.Action("show", func() { // Actions define a single API endpoint together
		apidsl.Description("Get bottle by id") // with its path, parameters (both path
		// GET /bottles/:bottleID
		apidsl.Routing(apidsl.GET("/:bottleID"))      // parameters and querystring values) and payload
		apidsl.Params(func() {                 // (shape of the request body).
			apidsl.Param("bottleID", design.Integer, "Bottle ID")
		})
		// 200 OK
		apidsl.Response(design.OK)       // Responses define the shape and status code
		// 404 NotFound
		apidsl.Response(design.NotFound) // of HTTP responses.
	})
})

// BottleMedia defines the media type used to render bottles.
var BottleMedia = apidsl.MediaType("application/vnd.goa.example.bottle+json", func() {
	apidsl.Description("A bottle of wine")
	apidsl.Attributes(func() { // Attributes define the media type shape.
		apidsl.Attribute("id", design.Integer, "Unique bottle ID")
		apidsl.Attribute("href", design.String, "API href for making requests on the bottle")
		apidsl.Attribute("name", design.String, "Name of wine")
		apidsl.Attribute("addparam", design.String, "add param")
		apidsl.Required("id", "href", "name")
	})
	apidsl.View("default", func() { // View defines a rendering of the media type.
		apidsl.Attribute("id")   // Media types may have multiple views and must
		apidsl.Attribute("href") // have a "default" view.
		apidsl.Attribute("name")
		apidsl.Attribute("addparam")
	})
})
