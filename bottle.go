package main

import (
	"bitbucket.org/yoshii/goa_sample/app"
	"github.com/goadesign/goa"
	"fmt"
)

// BottleController implements the bottle resource.
type BottleController struct {
	*goa.Controller
}

// NewBottleController creates a bottle controller.
func NewBottleController(service *goa.Service) *BottleController {
	return &BottleController{Controller: service.NewController("BottleController")}
}

// Show runs the show action.
func (c *BottleController) Show(ctx *app.ShowBottleContext) error {
	// BottleController_Show: start_implement

	var parameter int

	parameter = 1
	fmt.Printf("%d", &parameter)
	// Put your logic here
	fmt.Printf("%+v\n", ctx.Payload)

	fmt.Printf("%+v\n", ctx.Params)
	fmt.Printf("%+v\n", ctx.Params["bottleID"][0])

	ap := "added parameter"
	res := &app.GoaExampleBottle{
		Name: "aaaaaaaaaaaaaaaaa",
		ID: 100,
		Href: "hrefhrefhrefhref",
		Addparam: &ap,
	}
	return ctx.OK(res)
	// BottleController_Show: end_implement
}
